/**
 * Created by bbwansha on 16/6/28.
 */
var menu_one = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new two();
        this.addChild(layer);

    }
});
var two=cc.Layer.extend(
    {
        gear1_image:null,
        gear2_image:null,
        gear3_image:null,
        righton_image:null,
        spin:null,
        ctor: function () {
            this._super();
            var size=cc.director.getWinSize();
            cc.audioEngine.playMusic("res/gear.mp3",true);
            this.create_layout();
            gear_num=0;
        },
        create_layout:function()
        {
            var snowBackground=new cc.Sprite("res/gear/snowBackground.png");
            snowBackground.setPosition(569,320);
            this.addChild(snowBackground);
            var background2=new cc.Sprite("res/gear/background2.png");
            background2.setPosition(569,320);
            this.addChild(background2);
            var snowGearCover=new cc.Sprite("res/gear/snowGearCover.png");
            snowGearCover.setPosition(559.5,379);
            this.addChild(snowGearCover);
            this.gear1_image=new cc.MenuItemImage("#gear1on.png","#gear1on.png","#gear1off.png",this.gear_click,this);
            this.gear1_image.tag=1000;
            var gear1=new cc.Menu(this.gear1_image);
            gear1.setPosition(292,114);
            this.addChild(gear1);

            this.gear2_image=new cc.MenuItemImage("#gear2on.png","#gear2on.png","#gear2off.png",this.gear_click,this);
            this.gear2_image.tag=1001;
            var gear2=new cc.Menu(this.gear2_image);
            gear2.setPosition(568.5,114);
            this.addChild(gear2);

            this.gear3_image=new cc.MenuItemImage("#gear3on.png","#gear3on.png","#gear3off.png",this.gear_click,this);
            this.gear3_image.tag=1002;
            var gear3=new cc.Menu(this.gear3_image);
            gear3.setPosition(847,114);
            this.addChild(gear3);

            this.righton_image=new cc.MenuItemImage("#rightOn.png","#rightOff.png",this.push_game,this);
            this.righton_image.tag=1003;
            var rigtton=new cc.Menu(this.righton_image);
            rigtton.setPosition(1074.5,592.5);
            this.addChild(rigtton);
        },
        gear_click:function(sender)
        {
            sender.setEnabled(false);
            cc.audioEngine.playEffect("res/putGearUp.mp3");
            if(this.spin)
            {
                this.spin.removeFromParent();
            }
            if(sender.tag==1000)
            {
                this.gear2_image.setEnabled(true);
                this.gear3_image.setEnabled(true);
                gear_num=1;
                this.spin=new sp.SkeletonAnimation(spine_json_gear, spine_atlas_gear);
                this.spin.setPosition(528,379);
                this.spin.setAnimation(0,"animation",true);
                this.addChild(this.spin);
            }
            if(sender.tag==1001)
            {
                this.gear1_image.setEnabled(true);
                this.gear3_image.setEnabled(true);
                gear_num=2;
                this.spin=new sp.SkeletonAnimation(spine_json_gear2, spine_atlas_gear2);
                this.spin.setPosition(420,379);
                this.spin.setAnimation(0,"animation",true);
                this.addChild(this.spin);
            }
            if(sender.tag==1002)
            {
                this.gear1_image.setEnabled(true);
                this.gear2_image.setEnabled(true);
                gear_num=3;
                this.spin=new sp.SkeletonAnimation(spine_json_gear3, spine_atlas_gear3);
                this.spin.setPosition(575.5,379);
                this.spin.setAnimation(0,"animation",true);
                this.addChild(this.spin);
            }
        },
        push_game:function()
        {
           if(gear_num){
               cc.audioEngine.stopMusic();
               cc.director.runScene(new gear_one());
           }
               }
    });