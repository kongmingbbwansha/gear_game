/**
 * Created by bbwansha on 16/7/6.
 */
var startclient = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new start();
        this.addChild(layer);

    }
});
var start=cc.Layer.extend(
    {
        background:null,
        spin1:null,
        ctor: function () {
            this._super();
            cc.audioEngine.playEffect("res/loading.mp3",false);
            var size=cc.director.getWinSize();
            this.spin1=new sp.SkeletonAnimation(startanimation_json, startanimation_atlas);
            this.spin1.setAnimation(0,"animation",false);
            this.spin1.setPosition(size.width/2,size.height/2);
            this.addChild(this.spin1,1);

            this.background=new cc.Sprite("res/aaa/background.png");
            this.background.setPosition(size.width/2,size.height/2);
            this.addChild(this.background);
            this.scheduleOnce(function(){
                this.spin1.removeFromParent();
                this.background.removeFromParent();
                cc.audioEngine.playEffect("res/skip.mp3",false);
                var background1=new cc.Sprite("res/aaa/background1.png");
                background1.setPosition(size.width/2,size.height/2);
                this.addChild(background1);
                var button1_image=new cc.MenuItemImage("res/aaa/button1.png","res/aaa/button2.png",this.push,this);
                var menu=new cc.Menu(button1_image);
                menu.setPosition(886,96.5);
                this.addChild(menu);
            },3)

        },
        push:function()
        {
            cc.audioEngine.stopAllEffects();
            cc.director.runScene(new select_scene());
            cc.audioEngine.playEffect("res/selectLevel.mp3",false);
        }
    });