/**
 * Created by bbwansha on 16/6/29.
 */
var select_scene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new Three();
        this.addChild(layer);

    }
});
var Three=cc.Layer.extend(
    {
        arr_window:null,
        bool:null,
        delta:null,
        bg_tab1:null,
        ctor: function () {
            this._super();
            this.create_layout();
            this.click();
            var size=cc.director.getWinSize();


        },
        create_layout:function()
        {
            var background=new cc.Sprite("res/scene/background.png");
            background.setPosition(568,321.5);
            this.addChild(background);

            this.bg_tab1=new cc.LayerColor(cc.color(223,196,136),1500,477);
            this.bg_tab1.setOpacity(0);
            this.bg_tab1.setPosition(0,155);
            this.addChild(this.bg_tab1);

            var window_1=new cc.Sprite("res/scene/oneWindow.png");
            window_1.setPosition(258,238.5);
            this.bg_tab1.addChild(window_1);

            var window_2=new cc.Sprite("res/scene/oneWindow.png");
            window_2.setPosition(735,238.5);
            this.bg_tab1.addChild(window_2);

            var window_3=new cc.Sprite("res/scene/oneWindow.png");
            window_3.setPosition(1212,238.5);
            this.bg_tab1.addChild(window_3);

            var snow=new cc.Sprite("res/scene/snow.png");
            snow.setPosition(236.5,238.5);
            snow.setTag(1002);
            window_3.addChild(snow);

            var desert=new cc.Sprite("res/scene/desert.png");
            desert.setPosition(236.5,238.5);
            desert.setTag(1001);
            window_2.addChild(desert);

            var seaside=new cc.Sprite("res/scene/seaside.png");
            seaside.setPosition(236.5,238.5);
            seaside.setTag(1000);
            window_1.addChild(seaside);
            var road=new cc.Sprite("res/scene/road.png");
            road.setPosition(568,112);
            this.addChild(road);
            var man=new cc.Sprite("res/scene/man.png");
            man.setPosition(171.5,179.5);
            this.arr_window=[window_1,window_2,window_3];
            this.addChild(man);


        },
        click:function()
        {
            cc.eventManager.addListener(cc.EventListener.create({
                event : cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan : function(touch,event) {
                    var target = event.getCurrentTarget();
                    pos = touch.getLocation();
                    target.delta=0;
                   if(pos.y>224)
                   {
                       target.bool=1;
                       return true;
                   }
                    return false;
                },
                onTouchMoved : function(touch,event) {
                    var pos = touch.getLocation();
                    var target = event.getCurrentTarget();
                    var delta = touch.getDelta();
                    target.delta=delta;
                     if(delta.x>3||delta.x<-3)
                     {
                         target.bool=0;
                     }
                    target.bg_tab1.x +=delta.x;

                    if(target.bg_tab1.x<-500)
                    {
                        target.bg_tab1.x=-500;
                    }
                    if(target.bg_tab1.x>200)
                    {
                        target.bg_tab1.x=200;
                    }


                    return false;
                },
                onTouchEnded : function(touch,event) {
                    var pos = touch.getLocation();
                    var target = event.getCurrentTarget();
                    if(target.bool==0)
                    {
                        //target.slide(target.delta);
                    }
                   else {

                           var a=Math.floor((pos.x-target.bg_tab1.x)/477);
                        if(a>=0&&a<=2)
                        {
                            cc.log(a);
                            sence_num=a;
                            cc.log("跳转喽");
                            cc.director.runScene(new menu_one());
                        }

                    }

                }
            }), this);

        },
        slide:function(delta)
        {
            if(delta.x>0)
            {
                if(sence_num>0)
                {
                    for(var i=0;i<3;i++)
                    {
                     this.arr_window[i].x += 477;
                    }
                    sence_num--;
                }
            }else {
                if(sence_num<=1)
                {
                    for(var i=0;i<3;i++)
                    {
                        this.arr_window[i].x -= 477;
                    }
                    sence_num++;
                }
            }
            for(var i=0;i<3;i++)
            {
                this.arr_window[i].getChildByTag(1000+i).setScale(1,1);
            }
            this.arr_window[sence_num].getChildByTag(1000+sence_num).setScale(1.05,1.05);
        }
    });