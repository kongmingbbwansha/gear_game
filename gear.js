/**
 * Created by bbwansha on 16/6/16.
 */
var gear_one = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new one();
        this.addChild(layer);

    }
});
var one=cc.Layer.extend(
    {
        space: null,
        background:null,
        background1:null,
        arr_bg:null,
        near_bg:null,
        up_json:null,
        up1_json:null,
        H_json:null,
        xy_json:null,
        roadbed_arr:null,
        Segments:null,
        body_cycle:null,
        vel:0,
        shape1:null,
        shape:null,
        bool:1,
        bool1:0,
        bool2:1,
        spin1:null,
        body_cycle1:null,
        body_box:null,
        pin:null,
        json_data:null,
        homeIcon1_image:null,
        homeIcon1:null,
        homeIcon2_image:null,
        homeIcon2:null,
        refreshIcon1_image:null,
        refreshIcon1:null,
        refreshIcon1_image2:null,
        refreshIcon1:null,
        time:null,
        arr_force:null,
        arr_maxv:null,
        arr_maxtime:null,
        arr_music:null,
        ctor: function () {
            this._super();
            var size=cc.director.getWinSize();
            this.json_data=JSON.parse(json_data);
            this.up_json=JSON.parse(json_shangpo);
            this.H_json=JSON.parse(json_zhilu);
            this.up1_json=JSON.parse(json_shangpo1);
            this.roadbed_arr=[ this.up1_json,this.up_json,this.H_json];
            this.arr_bg=[];
            this.Segments=[];
            this.arr_force=[200,120,160];
            this.arr_maxv=[300,500,400];
            this.arr_maxtime=[26.21,37.44,34.96];
            this.arr_music=["res/seasideMusic.mp3","res/desertMusic.mp3","res/snowMusic.mp3"];
            cc.audioEngine.playMusic(this.arr_music[sence_num],true);
            var sprite_bg=function(pic,widch,height)
            {
                var layer=new cc.Layer();
                var sprit=new cc.Sprite(pic);
                sprit.x=widch/2;
                sprit.y=height/2;
                layer.addChild(sprit);
                var sprit1=new cc.Sprite(pic);
                sprit1.x=widch/2 +widch;
                sprit1.y=height/2;
                layer.addChild(sprit1);
                return layer;
            }
            cc.log(this.json_data.data[sence_num].road.length);
            this.background=sprite_bg(this.json_data.data[sence_num].pic.pic_1,1680,640);
            this.addChild(this.background);
            this.background.setPosition(0,0);
            var Speed=new cc.Sprite("res/Speed.png");
            Speed.setPosition(166.5,541);
            Speed.setName("speed");
            this.background.addChild(Speed,1);
            var lable_num=new cc.LabelTTF("000","Arial",32);
            lable_num.setPosition(228,500.5);
            lable_num.setName("lable");
            this.background.addChild(lable_num,1);
            this.background1=sprite_bg(this.json_data.data[sence_num].pic.pic_2,1680,640);
            this.background.addChild(this.background1);
            this.background1.setPosition(0,0);

            //胜利小旗
            var flag=new cc.Sprite("res/flag.png");
            flag.setPosition((this.json_data.data[sence_num].road.length -1)*1680,98);
            this.addChild(flag,10);

            //创建第三层背景的无限随机方法
            this.create_bg(this.json_data.data[sence_num].pic.pic_301,this.json_data.data[sence_num].pic.pic_302,this.json_data.data[sence_num].pic.pic_303,this.json_data.data[sence_num].pic.pic_304,this.arr_bg);
            this.initPhysics();
            //this.setupDebugNode();
            this.create_walls();
            this.scheduleUpdate();
            this.click();
            this.creat_Btn();
        },
        initPhysics: function ()
        {
            this.space = new cp.Space();
            this.space.gravity = cp.v(0, -100);
            this.space.sleepTimeThreshold = 2;
            this.space.collisionSlop=0.5;
        },
        setupDebugNode : function()
        {
            this._debugNode = new cc.PhysicsDebugNode(this.space );
            this._debugNode.visible = true ;
            this.addChild( this._debugNode );
        },
        update: function (dt)
        {
            var timeStep = 0.03;
            this.time+=dt;
            this.space.step(timeStep);
            this.x=-this.body_cycle.getPos().x +200;
            if(this.x>0)
            {
                this.x=0;
            }
            var size=cc.director.getWinSize();
            this.background.x=-this.x;

            this.background1.x+=-this.body_cycle.getVel().x *0.2*dt;
            if(this.background1.x<-1680)
            {
                this.background1.x=0;
            }
            //对第三层背景的操作
            for(var i=0;i<4;i++)
            {
                this.arr_bg[i].x+=-this.body_cycle.getVel().x *0.6*dt;
            }
            if(this.arr_bg[0].x<-1680)
            {
                var b=Math.floor(Math.random()*4);
                var ture=this.arr_bg[b].getTexture();
                var a=this.arr_bg[0];
                this.arr_bg.splice(0,1);
                a.setTexture(ture);
                a.x=5040;
                this.arr_bg.push(a);
            }
           //改变动画角度,并且判断是否翻车
          if(this.bool)
          {
              this.spin1.setPosition(this.body_box.getPosition().x+10,this.body_box.getPosition().y+3);
              if(this.body_cycle1.getPosition().x>this.body_cycle.getPosition().x)
              {
                  this.spin1.setRotation(-Math.atan((this.body_cycle1.getPosition().y-this.body_cycle.getPosition().y)/(this.body_cycle1.getPosition().x-this.body_cycle.getPosition().x))*59);
              }
              else {
                  this.bool=0;
                  this.spin1.removeFromParent();
                  for(var i=0;i<this.pin.length;i++)
                  {
                  this.space.removeConstraint(this.pin[i]);
                      cc.audioEngine.stopMusic();
                      this.scheduleOnce(function()
                      {
                          this.refres();
                      },2);
                  }
                  this.space.removeBody(this.body_box);
              }

          }
            //控制速度
            if(this.bool1)
            {
                this.body_cycle.resetForces();
                this.body_cycle.applyForce(cp.v(this.arr_force[gear_num-1],-100),cp.v(0,0));
                this.body_cycle.applyForce(cp.v(-(this.body_cycle.getVel().x/this.arr_maxv[gear_num-1])*this.arr_force[gear_num-1],0),cp.v(0,0));
            }
            if(!this.bool1)
            {
                if(this.body_cycle.getVel().x>0)
                {
                    this.body_cycle.resetForces();
                    this.body_cycle.applyForce(cp.v(-50,-100),cp.v(0,0));
                }
                if(this.body_cycle.getVel().x<0)
                {
                    this.body_cycle.resetForces();
                    this.body_cycle.applyForce(cp.v(20,-100),cp.v(0,0));
                }
            }

            var lable=this.background.getChildByName("lable");
            if(Math.floor(this.body_cycle.getVel().x)>=0)
            {
                lable.setString(Math.floor(this.body_cycle.getVel().x));
            }
            //成功界面
             if(this.body_cycle1.getPosition().x>(this.json_data.data[sence_num].road.length -1)*1680)
            {
                this.unscheduleUpdate();
                cc.audioEngine.playEffect("res/success.mp3",false);
                this.spin1.setVisible(false);
                var speed=this.background.getChildByName("speed");
                speed.setVisible(false);
                var lable=this.background.getChildByName("lable");
                lable.setVisible(false);
                this.homeIcon1_image.setVisible(false);
                this.refreshIcon1.setVisible(false);
                this.space.removeBody(this.body_cycle);
                this.space.removeBody(this.body_cycle1);
                this.space.removeBody(this.body_box);
                var a=this.getChildByName("phy1");
                a.removeFromParent();
                var b=this.getChildByName("phy2");
                b.removeFromParent();
                var success=new cc.Sprite("res/success.png");
                success.setAnchorPoint(cc.p(0.5,0.5));
                success.setPosition(size.width/2,size.height/2);
                this.background.addChild(success);
                var max_time=new cc.LabelTTF("35.83","Arial",58.65);
                max_time.setPosition(722,418);
                max_time.setColor(cc.color(255,213,0));
                max_time.setString(this.arr_maxtime[sence_num]);
                this.background.addChild(max_time);
                var you_time=new cc.LabelTTF("00:","Arial",68.23);
                you_time.setPosition(573.5,278);
                this.background.addChild(you_time);
                you_time.setColor(cc.color(255,168,0));
                you_time.setString(Math.floor(this.time*100)/100 );
                var home_image=new cc.MenuItemImage("res/home.png","res/home2.png",this.back_Menu,this);
                var home=new cc.Menu(home_image);
                home.setPosition(846.5,255);
                this.background.addChild(home);
                var refresh_image=new cc.MenuItemImage("res/refresh.png","res/refresh2.png",this.refres,this);
                var refresh=new cc.Menu(refresh_image);
                refresh.setPosition(749.5,165);
                this.background.addChild(refresh);
            }

        },
        //创建第三层背景
        create_bg:function(bg1,bg2,bg3,bg4,arr1)
        {
            var size=cc.director.getWinSize();
            var bg1=new cc.Sprite(bg1);
            bg1.setAnchorPoint(cc.p(0,0.5));
            bg1.setPosition(0,size.height/2);
            this.background.addChild(bg1);
            var bg2=new cc.Sprite(bg2);
            bg2.setAnchorPoint(cc.p(0,0.5));
            bg2.setPosition(1680,size.height/2);
            this.background.addChild(bg2);
            var bg3=new cc.Sprite(bg3);
            bg3.setAnchorPoint(cc.p(0,0.5));
            bg3.setPosition(3360,size.height/2);
            this.background.addChild(bg3);
            var bg4=new cc.Sprite(bg4);
            bg4.setAnchorPoint(cc.p(0,0.5));
            bg4.setPosition(5040,size.height/2);
            this.background.addChild(bg4);
            arr1.push(bg1);
            arr1.push(bg2);
            arr1.push(bg3);
            arr1.push(bg4);

        },
        create_walls:function()
        {
            for(var j=0;j<this.json_data.data[sence_num].road.length;j++)
            {
                cc.log(this.json_data.data[sence_num].road.length);
                for(var i=0;i<this.json_data.data[sence_num].road[j].lu.length -1;i++)
                {
                    var staticBody = new cp.StaticBody();
                    x1=this.json_data.data[sence_num].road[j].lu[i].x;
                    y1=this.json_data.data[sence_num].road[j].lu[i].y;
                    x2=this.json_data.data[sence_num].road[j].lu[i+1].x;
                    y2=this.json_data.data[sence_num].road[j].lu[i+1].y;
                    var Segment=new cp.SegmentShape(staticBody, cp.v(x1+j*1680, y1),
                        cp.v(x2+j*1680, y2), 0);
                    Segment.setElasticity(0);
                    Segment.setFriction(10);
                    this.space.addStaticShape(Segment);
                }
                var dimian=new cc.Sprite(this.json_data.data[sence_num].road[j].pic);
                dimian.setAnchorPoint(cc.p(0,0.5));
                dimian.setPosition(j*1680,320);
                this.addChild(dimian);
            }

            this.body_cycle=new cp.Body(1, cp.momentForCircle(1, 0, 53.5, cp.vzero));
            this.body_cycle.setPos(cp.v(200,300));
            this.space.addBody(this.body_cycle);
            this.shape=new cp.CircleShape(this.body_cycle,53.5,cp.vzero);
            this.shape.setElasticity(0);
            this.shape.setFriction(1);
            this.space.addShape(this.shape);
            var sprite=new cc.PhysicsSprite("res/frontWheel.png");
            sprite.setName("phy1");
            sprite.setBody(this.body_cycle);
            this.addChild(sprite);

            this.body_cycle1=new cp.Body(1, cp.momentForCircle(1, 0, 53.5, cp.vzero));
            this.body_cycle1.setPos(cp.v(352.5,300));
            this.space.addBody( this.body_cycle1);
            this.shape1=new cp.CircleShape(this.body_cycle1,53.5,cp.vzero);
            this.shape1.setElasticity(0);
            this.shape1.setFriction(1);
            this.space.addShape(this.shape1);
            var sprite1=new cc.PhysicsSprite("res/frontWheel.png");
            sprite1.setBody(this.body_cycle1);
            sprite1.setName("phy2");
            this.addChild(sprite1);

            this.body_box=new cp.Body(1, cp.momentForBox(1, 40,100));
            this.body_box.setPos(cp.v(276.5,400));
            this.space.addBody( this.body_box);
            var shape2=new cp.BoxShape(this.body_box, 40, 100);
            shape2.setElasticity(0);
            shape2.setFriction(1);
            this.space.addShape(shape2);

            this.spin1=new sp.SkeletonAnimation(Riding_json, Riding_atlas);
            this.spin1.setAnimation(0,"notride",true);
            this.spin1.setAnchorPoint(cc.p(0,0));
            this.spin1.setPosition(276.5,400);
            this.addChild(this.spin1);

            var pin=new cp.PinJoint(this.body_cycle,this.body_cycle1,cp.v(0,0),cp.v(0,0));
            var pin11=new cp.PinJoint(this.body_cycle,this.body_box,cp.v(0,0),cp.v(0,7));
            var pin1=new cp.PinJoint(this.body_cycle,this.body_box,cp.v(0,0),cp.v(0,-7));
            var pin2=new cp.PinJoint(this.body_cycle1,this.body_box,cp.v(0,0),cp.v(0,7));
            var pin22=new cp.PinJoint(this.body_cycle1,this.body_box,cp.v(0,0),cp.v(0,-7));
            this.space.addConstraint(pin);
            this.space.addConstraint(pin1);
            this.space.addConstraint(pin2);
            this.space.addConstraint(pin11);
            this.space.addConstraint(pin22);
            this.pin=[pin,pin11,pin1,pin2,pin22];


        },
        click:function()
        {
            cc.eventManager.addListener(cc.EventListener.create({
                event : cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan : function(touch,event) {
                    var target = event.getCurrentTarget();
                    pos = touch.getLocation();
                    target.body_cycle.applyForce(cp.v(target.arr_force[gear_num-1],-50),cp.v(0,0));
                    target.spin1.setAnimation(0,"riding",true);
                    target.bool1=1;
                    return true;
                },
                onTouchMoved : function(touch,event) {
                    var pos = touch.getLocation();
                    var target = event.getCurrentTarget();

                    return false;
                },
                onTouchEnded : function(touch,event) {
                    var pos = touch.getLocation();
                    var target = event.getCurrentTarget();

                    target.body_cycle.resetForces();
                    target.spin1.setAnimation(0,"notride",true);
                    target.bool1=0;
                }
            }), this);

        },
        creat_Btn:function()
        {
            this.homeIcon1_image=new cc.MenuItemImage("res/homeIcon1.png","res/homeIcon1.png",this.exchange,this);
            this.homeIcon1=new cc.Menu(this.homeIcon1_image);
            this.homeIcon1.setPosition(1083.5,590);
            this.background.addChild( this.homeIcon1);

            this.refreshIcon1_image=new cc.MenuItemImage("res/refreshIcon1.png","res/refreshIcon1.png",this.exchange1,this);
            this.refreshIcon1=new cc.Menu(this.refreshIcon1_image);
            this.refreshIcon1.setPosition(1010.5,590);
            this.background.addChild( this.refreshIcon1);

            this.homeIcon2_image=new cc.MenuItemImage("res/homeIcon2.png","res/homeIcon2.png",this.back_Menu,this);
            this.homeIcon2=new cc.Menu( this.homeIcon2_image);
            this.homeIcon2.setPosition(1083.5,590);
            this.homeIcon2.setVisible(false);
            this.background.addChild( this.homeIcon2);

            this.refreshIcon2_image=new cc.MenuItemImage("res/refreshIcon2.png","res/refreshIcon2.png",this.refres,this);
            this.refreshIcon2=new cc.Menu( this.refreshIcon2_image);
            this.refreshIcon2.setPosition(1010.5,590);
            this.refreshIcon2.setVisible(false);
            this.background.addChild( this.refreshIcon2);
        },


        exchange:function()
        {
            this.homeIcon1.setVisible(false);
            this.homeIcon2.setVisible(true);

            var action=cc.scaleBy(0.1,1.1,1.1);
            var sequence=cc.sequence(action,action.reverse());
            var repeat=cc.repeat(sequence,2);
            this.homeIcon2_image.runAction(repeat);


            this.scheduleOnce(function()
            {
                this.homeIcon1.setVisible(true);
                this.homeIcon2.setVisible(false);
                var action1=cc.scaleBy(0.1,1.1,1.1);
                var sequence1=cc.sequence(action1,action1.reverse());
                var repeat1=cc.repeat(sequence1,2);
                this.homeIcon1_image.runAction(repeat1);
            },2);
        },

        exchange1:function()
        {
            this.refreshIcon1.setVisible(false);
            this.refreshIcon2.setVisible(true);

            var action=cc.scaleBy(0.1,1.1,1.1);
            var sequence=cc.sequence(action,action.reverse());
            var repeat=cc.repeat(sequence,2);
            this.refreshIcon2_image.runAction(repeat);


            this.scheduleOnce(function()
            {
                this.refreshIcon1.setVisible(true);
                this.refreshIcon2.setVisible(false);

                var action1=cc.scaleBy(0.1,1.1,1.1);
                var sequence1=cc.sequence(action1,action1.reverse());
                var repeat1=cc.repeat(sequence1,2);
                this.refreshIcon1_image.runAction(repeat1);
            },2);
        },
        back_Menu:function()
        {
            cc.audioEngine.stopMusic();
            cc.director.runScene(new select_scene());
        },
        refres:function()
        {
            cc.audioEngine.stopMusic();
            cc.director.runScene(new gear_one());
        }

    }

);